<?php
include_once("modelo_bd_my.php");

class Llamadas extends modelo_mysql
{

//--------------------------------------------------------------------
//       Metodo Constructor de la clase
//--------------------------------------------------------------------
	public function Llamadas(){
  //inicalizacion de variables
		parent::modelo_mysql("localhost","telefono","root","20643647");
	}

	/**
	 * date = all ( todas las llamdas )
	 * */
	public function llamadasDiarias($date, $offset, $rowsPerPage)
	{
		if($date='all')
		{
			$sql = "SELECT id, nombres, cedula, telefono, hora_inicio, hora_final, estado FROM llamadas ORDER BY id DESC LIMIT $offset, $rowsPerPage";
		}
		else
		{
			$datei = $date." 00:00:00";
			$datef = $date." 23:59:59";
			$sql = "SELECT id, nombres, cedula, telefono, hora_inicio, hora_final, estado FROM llamadas WHERE hora_inicio BETWEEN '$datei' and '$datef' ORDER BY id DESC LIMIT $offset, $rowsPerPage";

		}
		$qry = parent::consulta($sql);
		while ($row = $this->getResultado($qry)){
			$rows[] = $row;
		}
		return $rows;
	}

	public function promedioLlamadas($datei, $datef)
	{
		$datei = $datei." 00:00:00";
		$datef = $datef." 23:59:59";
		$sql = "SELECT hora_inicio, hora_final, AVG(TIMESTAMPDIFF(SECOND, hora_inicio, hora_final ) ) AS promedio FROM llamadas WHERE hora_inicio BETWEEN '$datei' and '$datef'";
		$qry = parent::consulta($sql);
		while ($row = $this->getResultado($qry) ) {
			$rows[] = $row;
		}
		return $rows;
	}

	public function promedioLlamadasTotal()
	{
		//$datei = $datei." 00:00:00";
		//$datef = $datef." 23:59:59";
		$sql = "SELECT hora_inicio, hora_final, AVG(TIMESTAMPDIFF(SECOND, hora_inicio, hora_final ) ) AS promedio FROM llamadas";
		$qry = parent::consulta($sql);
		while ($row = $this->getResultado($qry) ) {
			$rows[] = $row;
		}
		return $rows;
	}	

	public function filtroEstado($estado, $offset, $rowsPerPage)
	{
		$sql = "SELECT id, nombres, cedula, telefono, hora_inicio, hora_final, estado FROM llamadas WHERE estado = '$estado' ORDER BY id DESC LIMIT $offset, $rowsPerPage" ;
		$qry = parent::consulta($sql);

		while ($row = $this->getResultado($qry) ) {
			$rows[] = $row;
		}
		return $rows;
	}

	public function cuentaFiltroEstado($estado)
	{
		$sql = "SELECT id , estado FROM llamadas WHERE estado = '$estado'";
		$qry = parent::consulta($sql);
		$count = $this->num_tuplas($qry);
		return $count;
	}

	public function llamadasEstados()
	{
		//$sql = "SELECT COUNT( estado ) cantidad, estado FROM llamadas where hora_inicio in NOW( )  GROUP BY estado";
		$sql = "SELECT  COUNT( estado ) cantidad, estado  FROM llamadas WHERE DATE(hora_inicio) = DATE(NOW( )) GROUP BY estado";
		$qry = parent::consulta($sql);
		while ($row = $this->getResultado($qry) ) {
			$rows[] = $row;
		}
		return $rows;
	}

public function llamadasUltimosEstados()
	{
		$sql = "SELECT COUNT( estado ) cantidad, estado, hora_inicio FROM llamadas WHERE hora_inicio BETWEEN DATE_SUB( NOW( ) , INTERVAL 5 DAY ) AND NOW( ) GROUP BY DATE(hora_inicio), estado";
		$qry = parent::consulta($sql);
		while ($row = $this->getResultado($qry) ) {
			$rows[] = $row;
		}
		return $rows;
	}
	/**
	 * funciones para paginar
	 *
	 * Cuenta nro de registros ( enviar all si se quieren todos los registros )
	 * date = all ( indica todas las llamadas )
	 *
	*/
	public function cuentaLlamadasDia($date)
	{
		if($date=='all')
		{
			$sql = "SELECT id FROM llamadas";
		}
		else
		{
			$datei = $date." 00:00:00";
			$datef = $date." 23:59:59";
			$sql = "SELECT id FROM llamadas WHERE hora_inicio BETWEEN '$datei' and '$datef'";
		}
		$qry = parent::consulta($sql);
		$count = $this->num_tuplas($qry);
		return $count;
	}

}
