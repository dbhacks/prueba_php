<?php
//--------------------------------------------------------------------
//       Clase modelo_mysql Capa Datos
//--------------------------------------------------------------------
class modelo_mysql
{ 
	var $conexion;
	var $servidor;
	var $basedato;
	var $usuario;
	var $clave;
//--------------------------------------------------------------------
//       Constructor
//--------------------------------------------------------------------
	function modelo_mysql($servidor,$basedato,$usuario,$clave) 
	{
		$this->servidor   = 'localhost';
		$this->basedato   = 'telefono';
		$this->usuario    = 'root';
		$this->clave      = '20643647';
	}	
//--------------------------------------------------------------------
//       Abrir conexi�n o conectar
//--------------------------------------------------------------------
	function crearConexion() 
	{
		$this->conexion = mysqli_connect($this->servidor,$this->usuario,$this->clave) or die('No se conecta con  M Y S Q L' );		
		if ( $this->conexion && mysqli_select_db( $this->conexion,$this->basedato) ){
			return true;
		}
		else  
		{
			return false;
		}	
	}
//--------------------------------------------------------------------
//       Ejecutar una sentencia SQL 
//--------------------------------------------------------------------
	function consulta($query) 
	{
		if ( $this->crearConexion() )
			return ( mysqli_query($this->conexion, $query) );
		else
			return null;
	}
//--------------------------------------------------------------------
//       Proxima Tupla o regsitro de una Tabla
//--------------------------------------------------------------------
	function getResultado($resultado) 
	{
		return mysqli_fetch_array($resultado);
	}
//--------------------------------------------------------------------
//       Cantidad de Tuplas o regsitros de una consulta
//--------------------------------------------------------------------
	function num_tuplas($resultado) 
	{
		return mysqli_num_rows($resultado);
	}
//--------------------------------------------------------------------
//       Cierra la conexi�n 
//--------------------------------------------------------------------
	function cerrar_bd() 
	{
		@mysqli_close($this->conexion);
	} 
//--------------------------------------------------------------------
//       Transforma la fecha del formato actual al formato de mysql a-m-d
//--------------------------------------------------------------------
	function fecha_bd($Fecha)
	{
		$unaFecha="now()";
		if (strlen($Fecha)==10)
		{
			$elDia=substr($Fecha,0,2);
			$elMes=substr($Fecha,3,2);
			$elYear=substr($Fecha,6,4);
			$FechaBD=$elYear."-".$elMes."-".$elDia;
		}
		return $FechaBD;
	}
}
//  Fin de la clase
