<?php 
require_once("../modelo/modelo_llamadas.php");
$objLlamadas = new Llamadas();

$data = $objLlamadas->llamadasUltimosEstados();

$json = json_encode($data);
header('Content-type: application/json');
echo $json;
