<?php
//Si hay registros
if ($num_total_registros > 0) {    ?>

<div class="row">
	<div class="span12">
		<div class="widget big-stats-container stacked">
			<div class="widget-content">
			    <div id="big_stats" class="cf">
                    <div class="stat">
                        <h4>Total Llamadas Hoy </h4>
                        <span class="value"><?= $llamadas_hoy; ?></span>
                    </div>
					<div class="stat">
						<h4>Total Llamadas al Día</h4>
						<span class="value"><?= $num_total_registros; ?></span>
					</div>
					<!-- .stat -->
					<div class="stat">
						<h4>Tiempo Promedio de Llamadas</h4>
						<span class="value"><?= $pro; ?></span>
					</div>
					<!-- .stat -->
				</div>
			</div>
			<!-- /widget-content -->
		</div>
		<!-- /widget -->
	</div>
	<!-- /span12 -->
</div>
<!-- /row -->
<?php 
}
else
{
	echo 'No hay Información';

}
?>
