<?php
require_once("../modelo/modelo_llamadas.php");

$objLlamadas = new Llamadas();
$date = date("Y-m-d");
$num_total_registros = $objLlamadas->cuentaLlamadasDia('all');
$llamadas_hoy = $objLlamadas->cuentaLlamadasDia($date);
$promedios = $objLlamadas->promedioLlamadasTotal();
foreach ($promedios as $promedio) {
	$pro = $promedio['promedio'];
}

?>
<div class="row">
	<div class="span6">
		<div class="widget stacked">
			<div class="widget-header">
				<i class="icon-star"></i>
                <h3>Llamadas Totales del Dia ( <?= $llamadas_hoy; ?> )</h3>
            </div>
            <!-- /widget-header -->
			<div class="widget-content">
				<div id="pie-chart" class="chart-holder"></div>
			</div>
            <!-- /widget-content -->
		</div>
        <!-- /widget -->
	</div>
    <!-- /span6 -->

<div class="span6">
	<div class="widget stacked">
		<div class="widget-header">
			<i class="icon-list-alt"></i>
			<h3>Llamadas del Dia</h3>
        </div>
		<!-- /widget-header -->
		<div class="widget-content">
			<div id="bar-chart" class="chart-holder"></div>
		</div>
		<!-- /widget-content -->
	</div>
    <!-- /widget -->
</div>
<!-- /span6 -->
</div>
<!-- /row -->
