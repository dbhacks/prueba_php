<?php 
require_once("../modelo/modelo_llamadas.php");
$objLlamadas = new Llamadas();

$data = $objLlamadas->llamadasEstados();

$json = json_encode($data);
header('Content-type: application/json');
echo $json;