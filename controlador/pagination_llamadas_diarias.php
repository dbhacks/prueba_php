<?php

require_once("../modelo/modelo_llamadas.php");

$objLlamadas = new Llamadas();
$llamadas = $objLlamadas->llamadasDiarias('all', $offset=0, $rowsPerPage=1000);
?>
<table id="datatable" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>Cedula</th>
			<th>Nombres</th>
			<th>Telefono</th>
			<th>Hora Inicio</th>
			<th>Hora Fin</th>
			<th>Estado</th>
		</tr>	
	</thead>
	<tbody>
		<?php foreach ($llamadas as $llamada):
			switch ($llamada['estado']) {
				case '0':
					$estado= 'Usuario Registrado';
					break;
				case '1':
					$estado= 'LLamada Atendida';
					break;
				case '2':
					$estado= 'Llamada Culminada';
					break;
				case '3':
					$estado= 'Llamada Cancelada Asesor';
					break;
				case '4':
					$estado= 'Asesor No disponible';
					break;
				case '6':
					$estado= 'Llamada cancelada Cliente';
					break;
			}
		 ?>
			<tr>
				<td> <?= $llamada['cedula'] ?> </td>
				<td> <?= $llamada['nombres'] ?> </td>
				<td> <?= $llamada['telefono'] ?> </td>
				<td> <?= $llamada['hora_inicio'] ?> </td>
				<td> <?= $llamada['hora_final'] ?> </td>
				<td> <?= $estado ?> </td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>