<?php
session_start();
//--------------------------------------------------------------------
//       se llama a la funcion de acuerdo a la operacion
$ope = stripslashes($_POST['ope']);
$date = stripslashes($_POST['date']);
$datei = stripslashes($_POST['datei']);
$datef = stripslashes($_POST['datef']);
$estado = stripslashes($_POST['estado']);
include_once("../modelo/modelo_llamadas.php");
switch($ope){
	case "llamadasDiarias":
	{
		llamadasDiarias($date);
		break;
	}
	case "promedioDia":
	{
		promedioDia($datei, $datef);
		break;
	}
	case "filtroEstado":
	{
		filtroEstado($estado,$datei, $datef);
		break;
	}
	case "Incluir":	{
		incluir();
		break;
	}
	case "Buscar":	{
		buscar();
		break;
	}
	case "Modifica":	{
		modifica();
		break;
	}
	case "Elimina":	{
		elimina();
		break;
	}
}

function llamadasDiarias($date)
{
	$objLlamada = new Llamadas();
	$llamadas  = $objLlamada->llamadasDiarias($date);
	$_SESSION['data']= $llamadas;
	header("Location: ../index.php");
}

//Dada una fecha se realiza el promedio de llamadas en esa fecha
function promedioDia($datei, $datef)
{
	$objLlamada = new Llamadas();
	$promedio  = $objLlamada->promedioLlamadas($datei, $datef);
	$_SESSION['promedio']= $promedio;
	header("Location: ../index.php");
}
// Filtra dependiendo 
function filtroEstado($estado, $datei, $datef)
{
	$objLlamada = new Llamadas();
	$filtro = $objLlamada->filtroEstado($estado, $datei, $datef);
	$_SESSION['filtro']= $filtro;
	header("Location: ../index.php");
}

?>
