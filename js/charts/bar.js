$(function () {

	$.ajax({
		type: "GET",
		url:"../controlador/estados.php",
		async: true,
		success: function(datos){
			var dataJson = eval(datos);
			var data = new Array ();
			var ds = new Array();
			var elementos = new Array();
			var series = dataJson.length;

			for( var i = 0; i<series; i++)
			{	
				elementos[i] = [dataJson[i].estado, dataJson[i].cantidad]; 
				data.push([[dataJson[i].estado, dataJson[i].cantidad]]);
			}
			
    for (var i=0, j=data.length; i<j; i++) {
    	
	     ds.push({
	        data:data[i],
	        grid:{
            hoverable:true
        },
	        bars: {
	            show: true, 
	            barWidth: 0.2, 
	            order: 1,
	            lineWidth: 0.5, 
				fillColor: { colors: [ { opacity: 0.65 }, { opacity: 1 } ] }
	        }
	    });
	}
	    
    $.plot($("#bar-chart"), ds, {
    	colors: ["#F90", "#222", "#666", "#BBB", "#0A2984"],
    	legend: {
				show: true,
				noColumns: 1, // number of colums in legend table
				labelFormatter: null, // fn: string -> string
				labelBoxBorderColor: "#888", // border color for the little label boxes
				container: null, // container (as jQuery object) to put legend in, null means default on top of graph
				position: "ne", // position of default legend container within plot
				margin: [5, 10], // distance from grid edge to default legend container within plot
				backgroundOpacity: 0 // set to 0 to avoid background
			},
                

    });
			









},
error: function (obj, error, objError){
            //avisar que ocurrió un error
        }
    });



});
