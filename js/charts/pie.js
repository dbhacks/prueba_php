$(function () {

function isEmptyJSON(obj) {
  for(var i in obj) { return false; }
  return true;
}


	$.ajax({
		type: "GET",
		url:"../controlador/estados.php",
		async: true,
		success: function(datos){
			var dataJson = eval(datos);
			var data = [];
		
			if(isEmptyJSON(dataJson))
			{

			}
			var series = dataJson.length;

			for( var i = 0; i<series; i++)
			{
				if(dataJson[i].estado==1)
				{
					description = "Atendida";
				}
				else if(dataJson[i].estado==2)
				{
					description = "Culminada";
				}
				else if(dataJson[i].estado==3)
				{
					description = "Cancelada Operador";
				}
				else if(dataJson[i].estado==4)
				{
					description = "Asesor No disponible";
				}
				else if(dataJson[i].estado==6)
				{
					description = "Cancelada Cliente";
				}
				else if(dataJson[i].estado===0)
				{
					description = "Usuario Registrado";
				}
				data[i] = { label:"("+dataJson[i].cantidad+")"+description, data: parseInt(dataJson[i].cantidad)}
			}

			$.plot($("#pie-chart"), data, 
			{
				colors: ["#F90", "#B5B5B5", "#666", "#d97", "#0A2984"],
				series: {
					pie: { 
						show: true,
						label: {
							show: true,
							formatter: function(label, series){
								if(series==1){
									return '<div style="font-size:11px;text-align:center;padding:4px;color:black;">'+label+'<br/>'+'100'+'%</div>';
								}
								else
								{
									return '<div style="font-size:11px;text-align:center;padding:4px;color:black;">'+label+'<br/>'+Math.round(series.percent)+'%</div>';
								}
							},
							threshold: 0.1
						}
					}
				},
				
				legend: {
				show: true,
				noColumns: 1, // number of colums in legend table
				labelFormatter: null, // fn: string -> string
				labelBoxBorderColor: "#888", // border color for the little label boxes
				container: null, // container (as jQuery object) to put legend in, null means default on top of graph
				position: "ne", // position of default legend container within plot
				margin: [5, 10], // distance from grid edge to default legend container within plot
				backgroundOpacity: 0 // set to 0 to avoid background
			},
			grid: {
				hoverable: false,
				clickable: true
			},
		});

},
error: function (obj, error, objError){
            //avisar que ocurrió un error
        }
    });



});
