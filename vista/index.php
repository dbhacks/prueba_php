<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Reportes :: Llamadas</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">

    <link href="../css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet">

    <link href="../css/base-admin-2.css" rel="stylesheet">
    <link href="../css/base-admin-2-responsive.css" rel="stylesheet">

    <link href="../css/pages/reports.css" rel="stylesheet">

    <link href="../css/custom.css" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- datatable-->
    <link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    
</head>

<script type="text/javascript">
	$(document).ready(function(){
    $('#datatable').DataTable();
});

</script>

<body>

<div class="navbar navbar-inverse navbar-fixed-top">

    <div class="navbar-inner">

        <div class="container">

            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <i class="icon-cog"></i>
            </a>

            <a class="brand" href="./index.php">
                Reportes <sup>1.0</sup>
            </a>


        </div>
        <!-- /container -->

    </div>
    <!-- /navbar-inner -->

</div>
<!-- /navbar -->


<div class="subnavbar">

    <div class="subnavbar-inner">

        <div class="container">

            <a class="btn-subnavbar collapsed" data-toggle="collapse" data-target=".subnav-collapse">
                <i class="icon-reorder"></i>
            </a>

            <div class="subnav-collapse collapse">
                <ul class="mainnav">

                    <li class="">
                        <a href="./index.php">
                            <i class="icon-home"></i>
                            <span>Inicio</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /.subnav-collapse -->

        </div>
        <!-- /container -->

    </div>
    <!-- /subnavbar-inner -->

</div>
<!-- /subnavbar -->

<div class="main">

    <div class="container">
     <?php require('../controlador/widget_grafico.php'); ?>

     <?php require('../controlador/widget_totales.php'); ?>
        <div class="row">
            <div class="span12" id="central">
                <div class="widget stacked widget-table"> 
                    <div class="widget-header">
                        <span class="icon-file"></span>
                        <h3>Todas la llamadas paginadas</h3>
                    </div>
                    <div class="widget-content">
                        <div id="table_pagination">
                            <?php require('../controlador/pagination_llamadas_diarias.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- /container -->
</div>
    <!-- /main -->


    <div class="footer">

        <div class="container">

            <div class="row">

                <div id="footer-copyright" class="span6">
                    &copy;
                </div>
                <!-- /span6 -->

                <div id="footer-terms" class="span6">

                </div>
                <!-- /.span6 -->

            </div>
            <!-- /row -->

        </div>
        <!-- /container -->

    </div>
    <!-- /footer -->

	
	

	


    <!--<script src="../js/libs/jquery-1.8.3.min.js"></script>-->
    <script src="../js/libs/jquery-ui-1.10.0.custom.min.js"></script>
    <script src="../js/libs/bootstrap.min.js"></script>

    <script src="../js/plugins/flot/jquery.flot.js"></script>
    <script src="../js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="../js/plugins/flot/jquery.flot.orderBars.js"></script>
    <script src="../js/plugins/flot/jquery.flot.resize.js"></script>


    <script src="../js/charts/pie.js"></script>
    <script src="../js/charts/bar.js"></script>


</body>
</html>
